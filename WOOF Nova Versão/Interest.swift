//
//  Interest.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 16/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import  UIKit

class Interest{
    
    var nome = ""
    var sexo = ""
    var niver = ""
    var vacina = ""
    var peso = Int()
    var numberOfMembers = 0
    var numberOfPosts = 0
    var featuredImage: UIImage!
    
    init(nome: String, sexo: String, featuredImage: UIImage!, niver: String, vacina: String, peso: Int){
        self.nome = nome
        self.sexo = sexo
        self.featuredImage = featuredImage
        self.niver = niver
        self.vacina = vacina
        self.peso = peso
        
        numberOfPosts = 1
        numberOfMembers = 1
    }
    
    static func createInterests() -> [Interest]{
        return[Interest(nome: "Bul", sexo:  "Femea", featuredImage: UIImage(named: "dog-1")!, niver: "17/Nov", vacina: "Terça", peso: 47 )]
    }
}

